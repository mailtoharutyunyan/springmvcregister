<%--
  Created by IntelliJ IDEA.
  User: rootkit
  Date: 7/21/2019
  Time: 6:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>Bootstrap 4 Register Form</title>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
</head>
<body>

<div class="jumbotron text-center">
    <h1>Registration application</h1>
    <a href="${pageContext.request.contextPath}/auth/register" class="btn btn-warning" role="button">Register</a>
    <a href="${pageContext.request.contextPath}/auth/login" class="btn btn-info" role="button">Login</a>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <h3>Column 1</h3>
            <p>Some Data</p>
        </div>
        <div class="col-sm-4">
            <h3>Column 2</h3>
            <p>Some Data</p>
        </div>
        <div class="col-sm-4">
            <h3>Column 3</h3>
            <p>Some Data</p>
        </div>
    </div>
</div>
</body>