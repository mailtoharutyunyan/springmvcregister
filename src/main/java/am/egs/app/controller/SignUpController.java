package am.egs.app.controller;

import am.egs.app.dto.PersonDto;
import am.egs.app.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/auth")
public class SignUpController {

    private final PersonService personService;

    @Autowired
    public SignUpController(PersonService personService) {
        this.personService = personService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String create(@ModelAttribute PersonDto personDto) {
        personService.create(personDto);
        return "redirect:/auth/login";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register() {
        return "register";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public PersonDto update(@PathVariable int id, @RequestBody PersonDto personDto) {
        return personService.edit(id, personDto);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<PersonDto> getAll() {
        return personService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public PersonDto getOne(@PathVariable int id) {
        return personService.find(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public PersonDto delete(@PathVariable int id) {
        return personService.delete(id);
    }


}
