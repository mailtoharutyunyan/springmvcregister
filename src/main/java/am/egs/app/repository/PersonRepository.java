package am.egs.app.repository;

import am.egs.app.entity.Person;

import java.util.List;

public interface PersonRepository extends CrudRepository<Person>{

    @Override
    Person add(Person person);

    @Override
    Person edit(int id, Person person);

    @Override
    Person delete(int id);

    @Override
    Person find(int id);

    @Override
    List<Person> findAll();
}
