package am.egs.app.repository;

import java.util.List;

public interface CrudRepository<T> {

    T add(T t);

    T edit(int id, T t);

    T delete(int id);

    T find(int id);

    List<T> findAll();

}
