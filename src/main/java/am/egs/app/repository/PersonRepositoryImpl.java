package am.egs.app.repository;

import am.egs.app.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonRepositoryImpl implements PersonRepository {

    private static final String ADD_QUERY = "INSERT INTO person(id,firstName,lastName,age,email,username,password) VALUES (?,?,?,?,?,?,?)";
    private static final String editQuery = "UPDATE person SET first_name=?,last_name=?,age=?,email=?,username=?,password=? WHERE id = ?";
    private static final String deleteQuery = "DELETE from person WHERE id = ? ";
    private static final String findQuery = "SELECT * FROM person where id = ?";
    private static final String findAllQuery = "SELECT * FROM person";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PersonRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override

    public Person add(Person person) {
        try {
            jdbcTemplate.update(ADD_QUERY,
                    person.getId(),
                    person.getFirstName(),
                    person.getLastName(),
                    person.getAge(),
                    person.getEmail(),
                    person.getUsername(),
                    person.getPassword()
            );
        } catch (Exception e) {
            return null;
        }
        return person;
    }

    @Override
    public Person edit(int id, Person person) {
        try {
            jdbcTemplate.update(editQuery,
                    person.getFirstName(),
                    person.getLastName(),
                    person.getAge(),
                    person.getAge(),
                    person.getEmail(),
                    person.getUsername(),
                    person.getPassword(),
                    id);

        } catch (Exception e) {
            return null;
        }
        return person;
    }

    @Override
    public Person delete(int id) {
        jdbcTemplate.update(deleteQuery, id);
        return null;
    }

    @Override
    public Person find(int id) {
        return jdbcTemplate.queryForObject(findQuery, new Object[]{id}, new BeanPropertyRowMapper<>(Person.class));
    }

    @Override
    public List<Person> findAll() {
        return jdbcTemplate.query(findAllQuery, new BeanPropertyRowMapper<>(Person.class));

    }

}
