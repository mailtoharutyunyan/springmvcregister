package am.egs.app.service;

import am.egs.app.dto.PersonDto;
import am.egs.app.entity.Person;

import java.util.List;

public interface PersonService {

    PersonDto create(PersonDto personDto);

    PersonDto edit(int id, PersonDto personDto);

    PersonDto delete(int id);

    PersonDto find(int id);

    List<PersonDto> findAll();
}
