package am.egs.app.service;

import am.egs.app.dto.PersonDto;
import am.egs.app.entity.Person;
import am.egs.app.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    @Autowired
    public PersonServiceImpl(@Qualifier("personRepositoryImpl") PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public PersonDto create(PersonDto personDto) {
        Person person = new Person();
        person.setUsername(personDto.getUsername());
        person.setPassword(personDto.getPassword());
        person.setEmail(personDto.getEmail());
        person.setFirstName(personDto.getFirstName());
        person.setLastName(personDto.getLastName());
        person.setAge(personDto.getAge());
        personRepository.add(person);
        return null;
    }

    @Override
    public PersonDto edit(int id, PersonDto personDto) {
        try {
            Person person = personRepository.find(id);
            person.setUsername(personDto.getUsername());
            person.setPassword(personDto.getPassword());
            person.setEmail(personDto.getEmail());
            person.setFirstName(personDto.getFirstName());
            person.setLastName(personDto.getLastName());
            person.setAge(personDto.getAge());
            personRepository.edit(id, person);
        } catch (Exception e) {
            System.out.println("User not found");
        }
        return personDto;
    }

    @Override
    public PersonDto delete(int id) {
        return null;
    }

    @Override
    public PersonDto find(int id) {
        Person person = personRepository.find(id);
        PersonDto personDto = new PersonDto();
        personDto.setFirstName(person.getFirstName());
        personDto.setLastName(person.getFirstName());
        personDto.setAge(person.getAge());
        personDto.setEmail(person.getEmail());
        personDto.setUsername(person.getUsername());
        personDto.setPassword(person.getPassword());
        return personDto;
    }

    @Override
    public List<PersonDto> findAll() {
        List<Person> personList = personRepository.findAll();
        List<PersonDto> personDtos = new ArrayList<>();

        for (int i = 0; i < personList.size(); i++) {
            personDtos.get(i).setFirstName(personList.get(i).getFirstName());
            personDtos.get(i).setLastName(personList.get(i).getLastName());
            personDtos.get(i).setAge(personList.get(i).getAge());
            personDtos.get(i).setUsername(personList.get(i).getUsername());
            personDtos.get(i).setPassword(personList.get(i).getPassword());
            personDtos.get(i).setEmail(personList.get(i).getEmail());
        }
        return personDtos;
    }
}
